$(window).on("load", function (e) {
    var mySwiper = new Swiper('.swiper-container', {
        effect: 'fade',
        loop: true,
        pagination: {
            el: '.swiper-pagination',
            clickable: true,
            dynamicBullets: true,
            dynamicMainBullets: 3,
        },
        autoplay: {
            delay: 3000,
        },
        speed: 3000,
    })
});
